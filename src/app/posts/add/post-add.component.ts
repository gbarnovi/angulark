import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import * as postActions from '../../store/actions/posts.action';
import { Post } from 'src/app/models/posts.model';

@Component({
  selector: 'app-post-add',
  templateUrl: './post-add.component.html',
  styleUrls: ['./post-add.component.scss']
})
export class PostAddComponent implements OnInit {
  post: Post = { title: null, body: null, userId: 1, id: null };
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
  }

  addNewPost() {
    this.store.dispatch({ type: postActions.ADD_POST, payload: this.post });
    this.post = { userId: 1, id: null, title: null, body: null };
  }

}
