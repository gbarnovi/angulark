import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Post } from '../models/posts.model';
import { AppState } from '../app.state';
import { Store, State, select } from '@ngrx/store';
import * as PostsActions from '../store/actions/posts.action';
import { User } from '../models/user.model';
import { GET_USERS } from '../store/actions/user.action';
import { selectUserById } from '../store/selectors/users.selector';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostsComponent implements OnInit, OnDestroy {
  posts: Post[];
  users: User[];
  username: User;
  subscriptions: { posts: Subscription, users: Subscription } = { posts: null, users: null };



  constructor(private el: ChangeDetectorRef, private store: Store<AppState>) {
  }


  ngOnInit() {
    this.store.dispatch({ type: PostsActions.GET_POSTS });
    this.subscriptions.posts = this.store.select("posts").subscribe((response: Post[]) => {
      this.posts = response;
      this.el.detectChanges();
    });

    this.store.dispatch({ type: GET_USERS });
    this.subscriptions.users = this.store.select("users").subscribe((response: User[]) => {
      this.users = response;
      this.el.detectChanges();
    })
  }

  ngOnDestroy() {
    this.subscriptions.posts.unsubscribe();
    this.subscriptions.users.unsubscribe();
  }


  displayUserName(input: number) {
    this.store.pipe(select(selectUserById(input))).subscribe(response => this.username = response)
    if (this.username != null) {
      return this.username.username;
    }
  }

  deletePost(postId: number) {
    this.store.dispatch({ type: PostsActions.DELETE_POST, payload: postId });
  }
}