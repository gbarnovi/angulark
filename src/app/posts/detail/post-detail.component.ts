import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import * as commentActions from 'src/app/store/actions/comment.action';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import * as postActions from 'src/app/store/actions/posts.action';
import { getPostById } from 'src/app/store/selectors/posts.selector';
import { Post } from 'src/app/models/posts.model';
import { PostComment } from 'src/app/models/comment.model';

@Component({
  selector: 'app-post-detailed',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit, OnDestroy {
  selectedPostId: number;
  post: Post = { id: null, title: null, body: null, userId: null };
  comments: PostComment[] = [];
  newComment: PostComment = { id: null, postId: null, body: '', email: '', name: '' };
  subscriptions: { posts: Subscription, comments: Subscription } = { posts: null, comments: null };
  constructor(private store: Store<AppState>, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.selectedPostId = this.route.snapshot.params.id;
    this.subscriptions.posts = this.store.pipe(select<any>(getPostById(this.selectedPostId))).subscribe((post: Post) => {
      if (!post) {
        this.store.dispatch({ type: postActions.GET_POSTS });
        return;
      }
      this.post = post;
    });

    this.store.dispatch({ type: commentActions.GET_POST_COMMENTS, payload: this.selectedPostId });
    this.subscriptions.comments = this.store.select('comments').subscribe(comments => { this.comments = comments });
  }

  addNewComment() {
    this.newComment.postId = this.selectedPostId;
    this.store.dispatch({ type: commentActions.ADD_COMMENT, payload: this.newComment });
  }

  ngOnDestroy() {
    this.subscriptions.comments.unsubscribe();
    this.subscriptions.posts.unsubscribe();
  }

}
