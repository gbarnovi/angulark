import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from 'src/app/app.state';
import { Post } from 'src/app/models/posts.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import * as postActions from '../../store/actions/posts.action';
import { getPostById } from 'src/app/store/selectors/posts.selector';

@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.scss']
})
export class PostEditComponent implements OnInit, OnDestroy {
  selectedPostId: number;
  postSubscription: Subscription;
  post: Post = { userId: null, id: null, title: null, body: null };
  constructor(private store: Store<AppState>, private route: ActivatedRoute) { }

  ngOnInit() {
    this.selectedPostId = this.route.snapshot.params.id;
    this.postSubscription = this.store.pipe(select<any>(getPostById(this.selectedPostId))).subscribe((post: Post) => {
      this.post = Object.assign(this.post, post);
    });
  }

  updatePost() {
    this.store.dispatch({ type: postActions.UPDATE_POST, payload: this.post });
    this.post = { userId: null, id: null, title: null, body: null };
  }

  ngOnDestroy() {
    this.postSubscription.unsubscribe();
  }

}
