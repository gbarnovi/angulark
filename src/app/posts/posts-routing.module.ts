import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts.component';
import { PostDetailComponent } from './detail/post-detail.component';
import { PostEditComponent } from './edit/post-edit.component';
import { PostAddComponent } from './add/post-add.component';


const routes: Routes = [
  { path: 'posts', component: PostsComponent },
  { path: 'posts/add', component: PostAddComponent },
  { path: 'posts/:id', component: PostDetailComponent },
  { path: 'posts/:id/edit', component: PostEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
