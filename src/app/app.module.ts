import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EffectsModule } from '@ngrx/effects';
import { SharedEffects } from './store/effects/shared.effects';
import { StoreModule } from '@ngrx/store';
import { reducer } from './store/reducers/post.reducer';
import { HttpClientModule } from '@angular/common/http';
import { PostsComponent } from './posts/posts.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { userReducer } from './store/reducers/user.reducer';
import { PostDetailComponent } from './posts/detail/post-detail.component';
import { PostEditComponent } from './posts/edit/post-edit.component';
import { commentReducer } from './store/reducers/comment.reducer';
import { FormsModule } from '@angular/forms';
import { PostAddComponent } from './posts/add/post-add.component';
import { AlbumComponent } from './album/album.component';
import { AlbumDetailComponent } from './album/detail/album-detail.component';
import { albumReducer } from './store/reducers/album.reducer';
import { photoReducer } from './store/reducers/photo.reducer';
import { TodoComponent } from './todo/todo.component';
import { todoReducer } from './store/reducers/todo.reducer';

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    PostDetailComponent,
    PostEditComponent,
    PostAddComponent,
    AlbumComponent,
    AlbumDetailComponent,
    TodoComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    EffectsModule.forRoot([SharedEffects]),
    StoreModule.forRoot({
      posts: reducer,
      users: userReducer,
      comments: commentReducer,
      albums: albumReducer,
      photos: photoReducer,
      todos: todoReducer
    }, {
        runtimeChecks: {
          strictStateImmutability: true,
          strictActionImmutability: true
        }
      }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
