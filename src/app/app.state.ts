import { Post } from './models/posts.model';
import { User } from './models/user.model';
import { PostComment } from './models/comment.model';
import { Album } from './models/album.model';
import { Photo } from './models/photo.model';
import { Todo } from './models/todo.model';

export interface AppState {
    posts: Post[];
    users: User[];
    comments: PostComment[];
    albums: Album[];
    photos: Photo[];
    todos: Todo[];
}
