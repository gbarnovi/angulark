import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { TodoComponent } from './todo/todo.component';
import { AlbumRoutingModule } from './album/album-routing.module';
import { PostsRoutingModule } from './posts/posts-routing.module';



const routes: Routes = [
  { path: 'todos', component: TodoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), PostsRoutingModule, AlbumRoutingModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
