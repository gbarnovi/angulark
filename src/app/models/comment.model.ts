export interface PostComment {
    id: number;
    postId: number;
    body: string;
    email: string;
    name: string;
}
