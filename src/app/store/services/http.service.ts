import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { PostComment } from '../../models/comment.model';
import { Post } from '../../models/posts.model';
import { Album } from '../../models/album.model';
import { Todo } from '../../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts');
  }

  addPost(post: Post) {
    return this.http.post('https://jsonplaceholder.typicode.com/posts', post);
  }

  updatePost(post: Post) {
    return this.http.put(`https://jsonplaceholder.typicode.com/posts/${post.id}`, post);
  }

  deletePost(id: number) {
    return this.http.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
  }


  getUsers() {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }

  getAlbums() {
    return this.http.get<Album[]>('https://jsonplaceholder.typicode.com/albums');
  }

  getTodos() {
    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos');
  }

  getPhoto() {
    return this.http.get<Album[]>('https://jsonplaceholder.typicode.com/photos');
  }


  getCommentsByPostId(postId: number) {
    return this.http.get<PostComment[]>(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`);
  }

  addComment(comment: PostComment) {
    return this.http.post('https://jsonplaceholder.typicode.com/comments', comment);
  }
}
