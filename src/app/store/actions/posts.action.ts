import { Action } from '@ngrx/store';
import { Post } from '../../models/posts.model';

export const GET_POSTS: string = "[POSTS] GET"
export const GET_POST: string = "[POSTS] GET ONE"
export const SET_POSTS_TO_STATE: string = "[POSTS] SET"

export const ADD_POST: string = "[POSTS] ADD"
export const ADD_POST_TO_STATE: string = "[POSTS] SET NEW"

export const UPDATE_POST: string = "[POSTS] UPDATE"
export const UPDATED_POST_TO_STATE: string = "[POSTS] SET UPDATE"

export const DELETE_POST: string = "[POSTS] DELETE"
export const DELETE_POST_FROM_STATE: string = "[POSTS] SET DELETE"


export class getOnePost implements Action {
    readonly type = GET_POST;
    constructor(public payload: number) { }
}

export class getAllTutorials implements Action {
    readonly type = GET_POSTS;
}

export class setLoadedPosts implements Action {
    readonly type = SET_POSTS_TO_STATE;
    constructor(public payload: Post[]) { }
}

export class deletePost implements Action {
    readonly type = DELETE_POST;
    constructor(public payload: number) { }
}

export class deletePostFromStore implements Action {
    readonly type = DELETE_POST_FROM_STATE;
    constructor(public payload: { id: number }) { }
}

export class AddNewPost implements Action {
    readonly type = ADD_POST;
    constructor(public payload: Post) { }
}

export class UpdatePost implements Action {
    readonly type = UPDATE_POST;
    constructor(public payload: Post) { }
}

export class UpdatePostState implements Action {
    readonly type = UPDATED_POST_TO_STATE;
    constructor(public payload: Post) { }
}

export type Action = getAllTutorials | setLoadedPosts | deletePost | AddNewPost;
