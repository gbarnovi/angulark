import { Action } from '@ngrx/store';
import { PostComment } from '../../models/comment.model';

export const GET_POST_COMMENTS: string = "[COMMENT] GET"
export const GET_COMMENT: string = "[COMMENT] GET ONE"
export const SET_COMMENTS_FOR_POST: string = "[COMMENT] SET"
export const ADD_COMMENT: string = "[COMMENT] ADD"
export const ADD_COMMENT_TO_STATE: string = "[COMMENT] SET NEW"



export class GetComments implements Action {
    readonly type = GET_POST_COMMENTS;
    constructor(public payload: number) { }
}

export class SetLoadedComments implements Action {
    readonly type = SET_COMMENTS_FOR_POST;
    constructor(public payload: PostComment[]) { }
}

export class AddNewComment implements Action {
    readonly type = ADD_COMMENT;
    constructor(public payload: PostComment) { }
}

export class AddNewCommentToState implements Action {
    readonly type = ADD_COMMENT_TO_STATE;
    constructor(public payload: PostComment) { }
}


export type Action = SetLoadedComments | GetComments | AddNewComment | AddNewCommentToState;
