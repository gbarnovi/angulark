import { User } from '../../models/user.model';
import { Action } from '@ngrx/store';


export const GET_USERS: string = "[USERS] GET"
export const SET_LOADED_USERS: string = "[USERS] SET"


export class getAllUsers implements Action {
    readonly type = GET_USERS;
}

export class setLoadedUsers implements Action {
    readonly type = SET_LOADED_USERS;
    constructor(public payload: User[]) { }
}

export type Action = getAllUsers | setLoadedUsers;
