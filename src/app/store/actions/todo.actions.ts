import { Action } from '@ngrx/store';
import { Todo } from '../../models/todo.model';

export const GET_TODOS: string = "[TODO] GET"
export const SET_TODOS_TO_STATE: string = "[TODO] SET"


export class GetTodo implements Action {
    readonly type = GET_TODOS;
    constructor() { }
}

export class SetTodo implements Action {
    readonly type = SET_TODOS_TO_STATE;
    constructor(public payload: Todo[]) { }
}

export type Action = GetTodo | SetTodo;
