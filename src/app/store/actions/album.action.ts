import { Action } from '@ngrx/store';
import { Album } from '../../models/album.model';
import { Photo } from '../../models/photo.model';

export const GET_ALBUM: string = "[ALBUM] GET"
export const SET_ALBUM_TO_STATE: string = "[ALBUM] SET"

export const GET_PHOTO: string = "[PHOTO] GET"
export const SET_PHOTO_TO_STATE: string = "[PHOTO] SET"



export class GetAlbum implements Action {
    readonly type = GET_ALBUM;
    constructor(public payload: number) { }
}

export class SetLoadedAlbumToState implements Action {
    readonly type = SET_ALBUM_TO_STATE;
    constructor(public payload: Album[]) { }
}


export class GetPhoto implements Action {
    readonly type = GET_PHOTO;
    constructor(public payload: number) { }
}

export class SetLoadedPhotosToState implements Action {
    readonly type = SET_PHOTO_TO_STATE;
    constructor(public payload: Photo[]) { }
}

export type Action = GetAlbum | SetLoadedAlbumToState | GetPhoto | SetLoadedPhotosToState;
