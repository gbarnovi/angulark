import { createSelector } from '@ngrx/store';
import { selectState } from './state.selector';

export const getPostById = (id) => createSelector(
    selectState.postsState,
    (post) => post.find((e) => e.id == parseInt(id))
);