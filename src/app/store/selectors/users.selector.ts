import { createSelector } from '@ngrx/store';
import { selectState } from './state.selector';

export const selectUserById = (id: number) => createSelector(
    selectState.usersState,
    function (users) { if (users != undefined) { return users.find((e) => e.id == id) } else { return null } }
);
