import { AppState } from '../../app.state';

const postsState = (state: AppState) => state.posts;
const usersState = (state: AppState) => state.users;
const photosState = (state: AppState) => state.photos;
const albumsState = (state: AppState) => state.albums;

export const selectState = { postsState, usersState, photosState, albumsState };