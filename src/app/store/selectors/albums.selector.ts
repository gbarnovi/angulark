import { createSelector } from '@ngrx/store';
import { selectState } from './state.selector';

export const selectAlbumById = (id: number) => createSelector(
    selectState.albumsState,
    function (album) { if (album != undefined) { return album.find((e) => e.id == id) } else { return null } }
);

export const selectPhotosByAlbumId = (id: number) => createSelector(
    selectState.photosState,
    (photos) => {
        if (photos != undefined) {
            return photos.filter((photo) => photo.albumId == id)
        } else { return null }
    }
);