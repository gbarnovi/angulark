import * as AlbumActions from '../actions/album.action'
import { Album } from '../../models/album.model';

export function albumReducer(state: Album[] = [], action) {
    switch (action.type) {
        case AlbumActions.GET_PHOTO:
            return state;
        case AlbumActions.SET_ALBUM_TO_STATE:
            return [...action.payload];
        default:
            return state;

    }
}