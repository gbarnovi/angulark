import * as TodoActions from '../actions/todo.actions';
import { Todo } from '../../models/todo.model';


export function todoReducer(state: Todo[] = [], action) {
    switch (action.type) {
        case TodoActions.GET_TODOS:
            return state;
        case TodoActions.SET_TODOS_TO_STATE:
            return [...action.payload];
        default:
            return state;

    }
}