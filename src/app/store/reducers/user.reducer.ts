import * as userActions from './../actions/user.action'
import { User } from '../../models/user.model';


export function userReducer(state: User[] = [], action) {
    switch (action.type) {
        case userActions.GET_USERS:
            return state;
        case userActions.SET_LOADED_USERS:
            return [...action.payload];
        default:
            return state;

    }
}