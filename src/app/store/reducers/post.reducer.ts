import { Post } from '../../models/posts.model';
import * as PostActions from './../actions/posts.action';

export function reducer(state: Post[] = [], action) {
    switch (action.type) {
        case PostActions.GET_POSTS:
            return state;
        case PostActions.SET_POSTS_TO_STATE:
            return [...action.payload];
        case PostActions.GET_POST:
            return state.find((e) => e.id == action.payload);
        case PostActions.ADD_POST_TO_STATE:
            return [...state, action.payload];
        case PostActions.UPDATED_POST_TO_STATE:
            return state.map(post => {
                if (post.id === action.payload.id) {
                    return Object.assign({}, action.payload);
                }
                return post;
            });
        case PostActions.DELETE_POST_FROM_STATE:
            return state.filter(post => post.id !== action.payload);
        default:
            return state;
    }
}
