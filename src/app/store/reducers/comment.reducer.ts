import * as commentActions from './../actions/comment.action';
import { PostComment } from '../../models/comment.model';


export function commentReducer(state: PostComment[] = [], action) {
    switch (action.type) {
        case commentActions.SET_COMMENTS_FOR_POST:
            return [...action.payload];
        case commentActions.GET_COMMENT:
            return state.find((postComment: PostComment) => postComment.id == action.payload);
        case commentActions.ADD_COMMENT:
            return state;
        case commentActions.ADD_COMMENT_TO_STATE:
            return [...state, action.payload];
        default:
            return state;
    }
}
