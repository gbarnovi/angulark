import * as AlbumActions from '../actions/album.action'
import { Photo } from '../../models/photo.model';


export function photoReducer(state: Photo[] = [], action) {
    switch (action.type) {
        case AlbumActions.GET_ALBUM:
            return state;
        case AlbumActions.SET_PHOTO_TO_STATE:
            return [...action.payload];
        default:
            return state;

    }
}