import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpService } from '../services/http.service';
import * as postActions from '../actions/posts.action';
import * as commentActions from '../actions/comment.action';
import *  as userActions from '../actions/user.action';
import { map, catchError, switchMap } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { PostComment } from '../../models/comment.model';
import { GET_ALBUM, SET_ALBUM_TO_STATE, SET_PHOTO_TO_STATE, GET_PHOTO } from '../actions/album.action';
import { GET_TODOS, SET_TODOS_TO_STATE } from '../actions/todo.actions';

@Injectable()
export class SharedEffects {
  loadPosts$ = createEffect(() => this.actions$.pipe(
    ofType(postActions.GET_POSTS),
    switchMap(() => this.HttpService.getPosts()
      .pipe(
        map(posts => ({ type: postActions.SET_POSTS_TO_STATE, payload: posts })),
        catchError(() => EMPTY)
      ))
  )
  );

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(userActions.GET_USERS),
    switchMap(() => this.HttpService.getUsers()
      .pipe(
        map(users => ({ type: userActions.SET_LOADED_USERS, payload: users })),
        catchError(() => EMPTY)
      ))
  )
  );

  loadPostComments$ = createEffect(() => this.actions$.pipe(
    ofType(commentActions.GET_POST_COMMENTS),
    switchMap((action: commentActions.GetComments) => this.HttpService.getCommentsByPostId(action.payload)
      .pipe(
        map((comments: PostComment[]) => ({ type: commentActions.SET_COMMENTS_FOR_POST, payload: comments })),
        catchError(() => EMPTY)
      ))
  )
  );

  addPostComment$ = createEffect(() => this.actions$.pipe(
    ofType(commentActions.ADD_COMMENT),
    switchMap((action: commentActions.AddNewComment) => this.HttpService.addComment(action.payload)
      .pipe(
        map((response) => ({ type: commentActions.ADD_COMMENT_TO_STATE, payload: response })),
        catchError(() => EMPTY)
      ))
  )
  );

  addNewPost$ = createEffect(() => this.actions$.pipe(
    ofType(postActions.ADD_POST),
    switchMap((action: postActions.AddNewPost) => this.HttpService.addPost(action.payload)
      .pipe(
        map((response) => ({ type: postActions.ADD_POST_TO_STATE, payload: response })),
        catchError(() => EMPTY)
      ))
  ));

  editPost$ = createEffect(() => this.actions$.pipe(
    ofType(postActions.UPDATE_POST),
    switchMap((action: postActions.UpdatePost) => this.HttpService.updatePost(action.payload)
      .pipe(
        map((response) => ({ type: postActions.UPDATED_POST_TO_STATE, payload: response })),
        catchError(() => EMPTY)
      ))
  ));


  deletePost$ = createEffect(() => this.actions$.pipe(
    ofType(postActions.DELETE_POST),
    switchMap((action: postActions.deletePost) => this.HttpService.deletePost(action.payload)
      .pipe(
        map((response) => ({ type: postActions.DELETE_POST_FROM_STATE, payload: action.payload })),
        catchError(() => EMPTY)
      ))
  ));

  getAlbums$ = createEffect(() => this.actions$.pipe(
    ofType(GET_ALBUM),
    switchMap(() => this.HttpService.getAlbums()
      .pipe(
        map((response) => ({ type: SET_ALBUM_TO_STATE, payload: response })),
        catchError(() => EMPTY)
      ))
  ));

  getPhotos$ = createEffect(() => this.actions$.pipe(
    ofType(GET_PHOTO),
    switchMap(() => this.HttpService.getPhoto()
      .pipe(
        map((response) => ({ type: SET_PHOTO_TO_STATE, payload: response })),
        catchError(() => EMPTY)
      ))
  ));

  getTodos$ = createEffect(() => this.actions$.pipe(
    ofType(GET_TODOS),
    switchMap(() => this.HttpService.getTodos()
      .pipe(
        map((response) => ({ type: SET_TODOS_TO_STATE, payload: response })),
        catchError(() => EMPTY)
      ))
  ));

  constructor(
    private actions$: Actions,
    private HttpService: HttpService,
  ) { }
}
