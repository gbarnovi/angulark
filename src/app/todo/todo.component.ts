import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppState } from '../app.state';
import { Store } from '@ngrx/store';
import { GET_TODOS } from '../store/actions/todo.actions';
import { Todo } from '../models/todo.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit, OnDestroy {
  todos: Todo[];
  todosSubscription: Subscription;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.dispatch({ type: GET_TODOS });
    this.todosSubscription = this.store.select('todos').subscribe((response) => { this.todos = response; });
  }

  ngOnDestroy() {
    this.todosSubscription.unsubscribe();
  }

}
