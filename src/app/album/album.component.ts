import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppState } from '../app.state';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { Album } from '../models/album.model';
import { GET_ALBUM, GET_PHOTO } from '../store/actions/album.action';
import { User } from '../models/user.model';
import { selectUserById } from '../store/selectors/users.selector';
import { selectPhotosByAlbumId } from '../store/selectors/albums.selector';
import { GET_USERS } from '../store/actions/user.action';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit, OnDestroy {
  subscriptions: { photos: Subscription, user: Subscription } = { photos: null, user: null };
  albums: Album[] = [];
  username: User;
  photosCount = 0;
  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.select('albums').subscribe((album) => { this.albums = album });
    this.store.dispatch({ type: GET_ALBUM });
    this.store.dispatch({ type: GET_USERS });
    this.store.dispatch({ type: GET_PHOTO });
  }

  displayUserName(input: number) {
    this.subscriptions.user = this.store.pipe(select(selectUserById(input))).subscribe(response => { this.username = response })
    if (this.username != null) {
      return this.username.username;
    }
  }

  displayPhotosCount(input: number) {
    this.subscriptions.photos = this.store.pipe(select(selectPhotosByAlbumId(input))).subscribe(response => { this.photosCount = response.length })
    return this.photosCount;
  }

  ngOnDestroy() {
    this.subscriptions.photos.unsubscribe();
    this.subscriptions.user.unsubscribe();
  }

}
