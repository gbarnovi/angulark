import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumDetailComponent } from './detail/album-detail.component';
import { AlbumComponent } from './album.component';


const routes: Routes = [
  { path: 'album', component: AlbumComponent },
  { path: 'album/:id', component: AlbumDetailComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumRoutingModule { }
