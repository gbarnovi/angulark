import { Component, OnInit, OnDestroy } from '@angular/core';
import { GET_PHOTO, GET_ALBUM } from '../../store/actions/album.action';
import { Store, select } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { AppState } from 'src/app/app.state';
import { Subscription } from 'rxjs';
import { Album } from 'src/app/models/album.model';
import { Photo } from 'src/app/models/photo.model';
import { selectAlbumById, selectPhotosByAlbumId } from 'src/app/store/selectors/albums.selector';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.scss']
})
export class AlbumDetailComponent implements OnInit, OnDestroy {
  selectedAlbumId: number;
  subscriptions: { photos: Subscription, albums: Subscription } = { photos: null, albums: null };
  album: Album = { title: null, userId: null, id: null };
  photos: Photo[] = [];
  largeImageUrl = '';
  constructor(private store: Store<AppState>, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.selectedAlbumId = this.route.snapshot.params.id;
    this.subscriptions.albums = this.store.pipe(select<any>(selectAlbumById(this.selectedAlbumId))).subscribe((response: Album) => {
      if (!response) {
        this.store.dispatch({ type: GET_ALBUM });
        return;
      }
      this.album = response;
    });

    this.subscriptions.photos = this.store.pipe(select<any>(selectPhotosByAlbumId(this.selectedAlbumId))).subscribe((response: Photo[]) => {
      if (!response || !response.length) {
        this.store.dispatch({ type: GET_PHOTO });
        return;
      }

      this.photos = response;
    });
  }

  showThisImageLarger(url: string) {
    this.largeImageUrl = url;
  }

  ngOnDestroy() {
    this.subscriptions.albums.unsubscribe();
    this.subscriptions.photos.unsubscribe();
  }
}
